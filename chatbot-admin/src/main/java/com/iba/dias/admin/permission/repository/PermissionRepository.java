package com.iba.dias.admin.permission.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iba.dias.admin.permission.model.Permission;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Integer> {
}
