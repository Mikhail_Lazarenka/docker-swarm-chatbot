package com.iba.dias.admin.action.builder;

import org.springframework.stereotype.Component;

import com.iba.dias.admin.action.dto.ActionDto;
import com.iba.dias.admin.action.model.Action;

@Component
public class ActionBuilder {
    public Action build(ActionDto actionDto){
        Action action = new Action();
        action.setId(actionDto.getId());
        action.setDateTime(actionDto.getDateTime());
        action.setUserName(actionDto.getUserName());
        action.setActionType(actionDto.getActionType());
        return action;
    }
}
