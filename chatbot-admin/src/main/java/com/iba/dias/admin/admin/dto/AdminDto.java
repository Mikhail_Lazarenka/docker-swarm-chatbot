package com.iba.dias.admin.admin.dto;

import java.util.List;

import com.iba.dias.admin.permission.dto.PermissionDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminDto {
    private Integer id;
    private String userName;
    private String email;
    private List<PermissionDto> permissionsDto;
}
