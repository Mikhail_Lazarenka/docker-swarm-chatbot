package com.iba.dias.admin.admin.builder;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.iba.dias.admin.admin.dto.AdminDto;
import com.iba.dias.admin.permission.builder.PermissionBuilder;
import com.iba.dias.admin.permission.dto.PermissionDto;
import com.iba.dias.admin.admin.model.Admin;
import com.iba.dias.admin.permission.model.Permission;

@Component
public class AdminBuilder {
    @Autowired
    private PermissionBuilder permissionBuilder;

    public Admin build(AdminDto adminDto) {
        Admin admin = new Admin();
        admin.setId(adminDto.getId());
        admin.setEmail(adminDto.getEmail());
        admin.setUserName(adminDto.getUserName());

        List<Permission> permissions = new ArrayList<>();
        List<PermissionDto> permissionsDto = adminDto.getPermissionsDto();
        permissionsDto.iterator().forEachRemaining(permissionDto -> permissions.add(permissionBuilder.build(permissionDto)));
        admin.setPermissions(permissions);
        return admin;
    }
}
