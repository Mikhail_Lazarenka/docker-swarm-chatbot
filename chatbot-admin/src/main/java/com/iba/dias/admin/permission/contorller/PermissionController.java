package com.iba.dias.admin.permission.contorller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.iba.dias.admin.permission.dto.PermissionDto;
import com.iba.dias.admin.permission.service.PermissionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins ="172.20.3.38:3000")
@RestController
@RequestMapping("/admin/permissions")
public class PermissionController {
    @Autowired
    private PermissionService service;


    @GetMapping
    @ResponseBody
    @ApiOperation(value = "Get Lists of permissions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get Lists of permissions successfully"),
            @ApiResponse(code = 400, message = "Can't get list permissions"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid List<PermissionDto> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get permission by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get permission by id successfully"),
            @ApiResponse(code = 400, message = "Can't get permission by id"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid PermissionDto findById(@ApiParam(value = "PermissionDto id from which permission object will retrieve", required = true)
    @PathVariable @Valid Integer id) {
        return service.findById(id);
    }

    @PostMapping
    @ResponseBody
    @ApiOperation(value = "Add an permission")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Add an permission successfully"),
            @ApiResponse(code = 400, message = "Can't add an permission"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid PermissionDto add(@ApiParam(value = "PermissionDto object store in database table", required = true)
    @RequestBody @Valid PermissionDto permission) {
        return service.add(permission);
    }

    @PutMapping
    @ResponseBody
    @ApiOperation(value = "Update an permission")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update an permission successfully"),
            @ApiResponse(code = 400, message = "Can't update an permission"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid PermissionDto update(@ApiParam(value = "Update permission object", required = true)
    @RequestBody @Valid PermissionDto permission) {
        return service.update(permission);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ApiOperation(value = "Delete an permission")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update an permission successfully"),
            @ApiResponse(code = 400, message = "Can't update an permission"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public void delete(@ApiParam(value = "Update permission object", required = true)
    @PathVariable @Valid Integer id) {
        service.delete(id);
    }
}
