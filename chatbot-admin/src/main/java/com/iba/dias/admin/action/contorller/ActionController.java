package com.iba.dias.admin.action.contorller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.iba.dias.admin.action.dto.ActionDto;
import com.iba.dias.admin.action.service.ActionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins ="172.20.3.38:3000")
@RestController
@RequestMapping("/admin/actions")
public class ActionController {
    @Autowired
    private ActionService service;


    @GetMapping
    @ResponseBody
    @ApiOperation(value = "Get Lists of actions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get Lists of actions successfully"),
            @ApiResponse(code = 400, message = "Can't get list actions"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid List<ActionDto> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get action by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get action by id successfully"),
            @ApiResponse(code = 400, message = "Can't get action by id"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid ActionDto findById(@ApiParam(value = "Action id from which action object will retrieve", required = true)
    @PathVariable @Valid Integer id) {
        return service.findById(id);
    }

    @PostMapping
    @ResponseBody
    @ApiOperation(value = "Add an action")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Add an action successfully"),
            @ApiResponse(code = 400, message = "Can't add an action"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid ActionDto add(@ApiParam(value = "Action object store in database table", required = true)
    @RequestBody @Valid ActionDto action) {
        return service.add(action);
    }

    @PutMapping
    @ResponseBody
    @ApiOperation(value = "Update an action")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update an action successfully"),
            @ApiResponse(code = 400, message = "Can't update an action"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid ActionDto update(@ApiParam(value = "Update action object", required = true)
    @RequestBody @Valid ActionDto action) {
        return service.update(action);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ApiOperation(value = "Delete an action")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update an action successfully"),
            @ApiResponse(code = 400, message = "Can't update an action"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public void delete(@ApiParam(value = "Update action object", required = true)
    @PathVariable @Valid Integer id) {
        service.delete(id);
    }

}
