package com.iba.dias.admin.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.iba.dias.admin.admin.model.Admin;
@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {
}
