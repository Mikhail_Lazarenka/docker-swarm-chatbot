package com.iba.dias.admin.admin.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iba.dias.admin.admin.builder.AdminBuilder;
import com.iba.dias.admin.admin.dto.AdminDto;
import com.iba.dias.admin.exception.ResourceNotFoundException;
import com.iba.dias.admin.admin.model.Admin;
import com.iba.dias.admin.admin.repository.AdminRepository;

@Service
public class AdminService {
    @Autowired
    private AdminRepository repository;
    @Autowired
    private AdminBuilder adminBuilder;

    private Admin merge(Admin admin, Admin updatedAdmin) {
        updatedAdmin.setId(admin.getId());
        updatedAdmin.setUserName(admin.getUserName());
        updatedAdmin.setEmail(admin.getEmail());
        updatedAdmin.setPermissions(admin.getPermissions());
        return updatedAdmin;
    }

    public @Valid List<AdminDto> findAll() {
        final List<Admin> admins = repository.findAll();
        List<AdminDto> adminsDto = new ArrayList<>();
        admins.iterator().forEachRemaining((permission -> adminsDto.add(permission.convertToDto())));
        return adminsDto;
    }

    public @Valid AdminDto findById(Integer id) {
        final Admin admin = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admin not found for this id " + id));
        return admin.convertToDto();
    }

    public AdminDto add(AdminDto adminDto) {
        final Admin admin = adminBuilder.build(adminDto);
        final Admin saved = repository.save(admin);
        return saved.convertToDto();
    }

    public AdminDto update(AdminDto adminUpdateDto) {
        final Integer id = adminUpdateDto.getId();
        final Admin admin = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admin not found for this id " + id));
        final Admin adminUpdate = adminBuilder.build(adminUpdateDto);
        final Admin mergedEntity = merge(adminUpdate, admin);
        repository.save(mergedEntity);
        return mergedEntity.convertToDto();
    }

    public void delete(Integer id) {
        final Admin admin = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admin not found for this id " + id));
        repository.delete(admin);
    }

}
