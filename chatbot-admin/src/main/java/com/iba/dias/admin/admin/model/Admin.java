package com.iba.dias.admin.admin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.iba.dias.admin.admin.dto.AdminDto;
import com.iba.dias.admin.permission.dto.PermissionDto;
import com.iba.dias.admin.permission.model.Permission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "admin")
public class Admin implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @NotNull
    private String userName;
    @NotNull
    private String email;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "admin_m2m_permission", joinColumns =  @JoinColumn(name = "admin_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private List<Permission> permissions;
    public AdminDto convertToDto(){
        AdminDto adminDto = new AdminDto();
        adminDto.setId(id);
        adminDto.setUserName(userName);
        adminDto.setEmail(email);
        List<PermissionDto> permissionsDto = new ArrayList<>();
        this.permissions.iterator().forEachRemaining((permission -> permissionsDto.add(permission.convertToDto())));
        adminDto.setPermissionsDto(permissionsDto);
        return adminDto;
    }
}
