package com.iba.dias.admin.admin.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.iba.dias.admin.admin.dto.AdminDto;
import com.iba.dias.admin.admin.service.AdminService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins ="172.20.3.38:3000")
@RestController
@RequestMapping("/admin/admins")
public class AdminController {
    @Autowired
    private AdminService service;

    @GetMapping
    @ResponseBody
    @ApiOperation(value = "Get Lists of admins")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get Lists of admins successfully"),
            @ApiResponse(code = 400, message = "Can't get list admins"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid List<AdminDto> findAll() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    @ApiOperation(value = "Get admin by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get admin by id successfully"),
            @ApiResponse(code = 400, message = "Can't get admin by id"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid AdminDto findById(@ApiParam(value = "Admin id from which admin object will retrieve", required = true)
    @PathVariable @Valid Integer id) {
        return service.findById(id);
    }

    @PostMapping
    @ResponseBody
    @ApiOperation(value = "Add an admin")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Add an admin successfully"),
            @ApiResponse(code = 400, message = "Can't add an admin"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid AdminDto add(@ApiParam(value = "Admin object store in database table", required = true)
    @RequestBody @Valid AdminDto admin) {
        return service.add(admin);
    }

    @PutMapping
    @ResponseBody
    @ApiOperation(value = "Update an admin")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update an admin successfully"),
            @ApiResponse(code = 400, message = "Can't update an admin"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @Valid AdminDto update(@ApiParam(value = "Update admin object", required = true)
    @RequestBody @Valid AdminDto admin) {
        return service.update(admin);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    @ApiOperation(value = "Delete an admin")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Update an admin successfully"),
            @ApiResponse(code = 400, message = "Can't update an admin"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public void delete(@ApiParam(value = "Update admin object", required = true)
    @PathVariable @Valid Integer id) {
        service.delete(id);
    }
}
