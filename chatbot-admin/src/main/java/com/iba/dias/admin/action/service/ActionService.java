package com.iba.dias.admin.action.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iba.dias.admin.action.builder.ActionBuilder;
import com.iba.dias.admin.action.dto.ActionDto;
import com.iba.dias.admin.exception.ResourceNotFoundException;
import com.iba.dias.admin.action.model.Action;
import com.iba.dias.admin.action.repository.ActionRepository;

@Service
public class ActionService {
    @Autowired
    private ActionRepository repository;
    @Autowired
    private ActionBuilder actionBuilder;

    private Action merge(Action action, Action updatedAction) {
        updatedAction.setActionType(action.getActionType());
        updatedAction.setDateTime(action.getDateTime());
        updatedAction.setId(action.getId());
        updatedAction.setUserName(action.getUserName());
        return updatedAction;
    }

    public @Valid List<ActionDto> findAll() {
        final List<Action> actions = repository.findAll();
        List<ActionDto> actionsDto = new ArrayList<>();
        actions.iterator().forEachRemaining((permission -> actionsDto.add(permission.convertToDto())));
        return actionsDto;
    }

    public @Valid ActionDto findById(Integer id) {
        final Action action = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Action not found for this id " + id));
        return action.convertToDto();
    }

    public ActionDto add(ActionDto actionDto) {
        final Action action = actionBuilder.build(actionDto);
        final Action save = repository.save(action);
        return save.convertToDto();
    }

    public ActionDto update(ActionDto actionUpdateDto) {
        Integer id = actionUpdateDto.getId();
        Action oldAction = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Action not found for this id " + id));
        Action actionUpdate = actionBuilder.build(actionUpdateDto);
        Action mergedEntity = merge(actionUpdate, oldAction);
        repository.save(mergedEntity);
        return  mergedEntity.convertToDto();
    }

    public void delete(Integer id) {
        final Action actionDto = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Action not found for this id " + id));
        repository.delete(actionDto);
    }

}
