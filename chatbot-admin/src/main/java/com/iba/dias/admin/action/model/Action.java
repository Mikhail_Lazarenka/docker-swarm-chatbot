package com.iba.dias.admin.action.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.iba.dias.admin.action.dto.ActionDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "action")
public class Action implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @NotNull
    private LocalDateTime dateTime;
    @NotNull
    private String userName;
    @NotNull
    private String actionType;

    public ActionDto convertToDto(){
        ActionDto actionDto = new ActionDto();
        actionDto.setId(id);
        actionDto.setDateTime(dateTime);
        actionDto.setUserName(userName);
        actionDto.setActionType(actionType);
        return actionDto;
    }
}
