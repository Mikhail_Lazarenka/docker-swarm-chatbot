package com.iba.dias.admin.permission.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iba.dias.admin.permission.builder.PermissionBuilder;
import com.iba.dias.admin.permission.dto.PermissionDto;
import com.iba.dias.admin.exception.ResourceNotFoundException;
import com.iba.dias.admin.permission.model.Permission;
import com.iba.dias.admin.permission.repository.PermissionRepository;

@Service
public class PermissionService {
    @Autowired
    private PermissionRepository repository;
    @Autowired
    private PermissionBuilder permissionBuilder;

    private Permission merge(Permission permission, Permission updatedPermission) {
        updatedPermission.setId(permission.getId());
        updatedPermission.setName(permission.getName());
        return updatedPermission;
    }

    public @Valid List<PermissionDto> findAll() {
        final List<Permission> permissions = repository.findAll();
        List<PermissionDto> permissionsDto = new ArrayList<>();
        permissions.iterator().forEachRemaining((permission -> permissionsDto.add(permission.convertToDto())));
        return permissionsDto;
    }

    public @Valid PermissionDto findById(Integer id) {
        final Permission permission = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Permission not found for this id " + id));
        return permission.convertToDto();
    }

    public PermissionDto add(PermissionDto permissionDto) {
        final Permission permission = permissionBuilder.build(permissionDto);
        final Permission added = repository.save(permission);
        return added.convertToDto();
    }

    public PermissionDto update(PermissionDto permissionUpdateDto) {
        Integer id = permissionUpdateDto.getId();
        Permission oldPermission = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Permission not found for this id " + id));
        Permission permissionUpdate = permissionBuilder.build(permissionUpdateDto);
        Permission mergedEntity = merge(permissionUpdate, oldPermission);
        repository.save(mergedEntity);
        return mergedEntity.convertToDto();
    }

    public void delete(Integer id) {
        final Permission permission = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Permission not found for this id " + id));
        repository.delete(permission);
    }
}
