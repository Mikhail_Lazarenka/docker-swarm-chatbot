package com.iba.dias.admin.permission.builder;

import org.springframework.stereotype.Component;

import com.iba.dias.admin.permission.dto.PermissionDto;
import com.iba.dias.admin.permission.model.Permission;

@Component
public class PermissionBuilder {
    public Permission build(PermissionDto permissionDto){
        Permission permission = new Permission();
        permission.setId(permissionDto.getId());
        permission.setName(permissionDto.getName());
        return permission;
    }
}
