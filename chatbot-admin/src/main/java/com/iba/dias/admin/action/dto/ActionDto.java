package com.iba.dias.admin.action.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActionDto {
    private Integer id;
    private LocalDateTime dateTime;
    private String userName;
    private String actionType;
}
