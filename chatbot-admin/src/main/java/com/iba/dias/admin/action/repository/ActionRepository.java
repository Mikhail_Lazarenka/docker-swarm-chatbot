package com.iba.dias.admin.action.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iba.dias.admin.action.model.Action;

public interface ActionRepository  extends JpaRepository<Action, Integer> {
    public Action findActionDtoByActionType(String actionType);
}
