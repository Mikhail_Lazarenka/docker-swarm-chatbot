package com.ibagroup.idas.chatbotserver.auth.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.ibagroup.idas.chatbotserver.auth.dto.Login;
import com.ibagroup.idas.chatbotserver.auth.dto.User;

@Service
public class AuthService {

    public User sendLoginRequest(Login login) throws Exception {

        try {
            User user = new User();

            // request url
            String url = "https://apps.iba.by/dioapi/v1/authenticate";

            // create an instance of RestTemplate
            RestTemplate restTemplate = new RestTemplate();

            // create headers
            HttpHeaders headers = new HttpHeaders();
            // set `content-type` header
            headers.setContentType(MediaType.APPLICATION_JSON);
            // set `accept` header
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            // request body parameters
            Map<String, Object> loginData = new HashMap<>();
            loginData.put("email", login.getEmail());
            loginData.put("password", login.getPassword());

            // build the request
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(loginData, headers);

            // send POST request
            ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);

            if (response.getStatusCode() == HttpStatus.OK) {

                String[] splitResponseByDots = splitResponseByDots(response);

                int indexOfSlashForUsername = splitResponseByDots[4].indexOf("/");
                int indexOfSlashForComma = splitResponseByDots[7].indexOf(",");

                user.setUsername(splitResponseByDots[4].substring(4, indexOfSlashForUsername - 1));
                user.setToken(splitResponseByDots[7].substring(1, indexOfSlashForComma - 1));
                user.setLogged(true);

            } else {
                user.setLogged(false);
            }
            return user;

        } catch (Exception e) {
            throw new Exception("Login data is not valid!");
        }
    }

    private String[] splitResponseByDots(ResponseEntity<String> response){
        try {
            return response.getBody().split(Pattern.quote(":"));
        } catch (Exception e) {
            throw new NullPointerException();
        }
    }

}
