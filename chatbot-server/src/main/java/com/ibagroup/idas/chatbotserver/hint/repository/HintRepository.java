package com.ibagroup.idas.chatbotserver.hint.repository;

import org.springframework.data.repository.CrudRepository;

public interface HintRepository extends CrudRepository<Hint,Integer> {


    Iterable<Hint> findByCategoryId(Integer categoryId);
}
