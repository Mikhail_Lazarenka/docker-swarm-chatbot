package com.ibagroup.idas.chatbotserver.category.service;

import com.ibagroup.idas.chatbotserver.category.controller.SubCategoryRs;
import com.ibagroup.idas.chatbotserver.category.controller.CategoryRq;
import com.ibagroup.idas.chatbotserver.category.converter.CategoriesToEntityConverter;
import com.ibagroup.idas.chatbotserver.category.converter.CategoryToRespConverter;
import com.ibagroup.idas.chatbotserver.category.repository.CategoryRepository;
import com.ibagroup.idas.chatbotserver.category.controller.CategoryResp;
import com.ibagroup.idas.chatbotserver.category.repository.CategoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoriesToEntityConverter categoriesToEntityConverter;
    @Autowired
    private CategoryToRespConverter categoryToRespConverter;

    public List<CategoryResp> getCategoryList() {
        Iterable<CategoryEntity> categories = categoryRepository.findByCategoryParentId(0);
        List<CategoryResp> categoryResps = new ArrayList<>();
        for (CategoryEntity categoryEntityRoot : categories) {
            Iterable<CategoryEntity> subCategories = categoryRepository.findByCategoryParentId(categoryEntityRoot.getId());
            List<SubCategoryRs> categoryChildRs = new ArrayList<>();
            subCategories.iterator().forEachRemaining(subCategory -> categoryChildRs.add(new SubCategoryRs(subCategory.getId(), subCategory.getName())));
            categoryResps.add(new CategoryResp(categoryEntityRoot.getId(), categoryEntityRoot.getName(), categoryChildRs));
        }
        return categoryResps;
    }

    public CategoryResp getCategoryById(Integer id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);
        // CategoryResp resp = new CategoryResp(category.get().getId(), category.get().getName(),  category.get().getDescription(), category.get().getCategoryParentId//());
        CategoryResp categoryResp = null;
        categoryResp = categoryToRespConverter.convert(categoryEntity);

        Iterable<CategoryEntity> subCategoriesEntity = categoryRepository.findByCategoryParentId(categoryEntity.get().getId());

        List<SubCategoryRs> subCategoryRs = new ArrayList<>();
        subCategoriesEntity.iterator().forEachRemaining(subCategory -> subCategoryRs.add(new SubCategoryRs(subCategory.getId(), subCategory.getName())));
        categoryResp.setSubCategories(subCategoryRs);
        return  categoryResp;
    }

    public SubCategoryRs getSubCategoryById(Integer subCategoryid, Integer categoryRootId) {
        CategoryEntity categories = categoryRepository.findByIdAndCategoryParentId(subCategoryid, categoryRootId);
        if(categories!= null) {
            SubCategoryRs categoryChildRs = new SubCategoryRs(categories.getId(), categories.getName());
            return categoryChildRs;
        }
        return null;
    }

    public void addCategories(List<CategoryRq> categories) {
        List<CategoryEntity> categoryEntities = categoriesToEntityConverter.convert(categories);
        categoryRepository.saveAll(categoryEntities);
    }



}
