package com.ibagroup.idas.chatbotserver.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ibagroup.idas.chatbotserver.auth.dto.Login;
import com.ibagroup.idas.chatbotserver.auth.dto.User;
import com.ibagroup.idas.chatbotserver.auth.service.AuthService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RequestMapping("/api/auth")
@RestController
@CrossOrigin
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    @ApiOperation(value = "Authorization service")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Logged"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public User login(@RequestBody Login login) throws Exception {
        return authService.sendLoginRequest(login);
    }

}



