package com.ibagroup.idas.chatbotserver.rating.controller;

import io.swagger.models.auth.In;

public class RatingRq {
    private Integer id;
    private Integer docId;
    private String mark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
