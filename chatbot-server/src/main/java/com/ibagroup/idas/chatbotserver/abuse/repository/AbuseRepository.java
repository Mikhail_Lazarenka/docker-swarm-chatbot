package com.ibagroup.idas.chatbotserver.abuse.repository;

import com.ibagroup.idas.chatbotserver.category.repository.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface AbuseRepository extends CrudRepository<CategoryEntity,Long> {
}
