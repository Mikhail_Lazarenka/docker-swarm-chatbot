package com.ibagroup.idas.chatbotserver.hint.controller;

public class HintResp {
    public HintResp() {
    }

    public HintResp(String link, String text, Integer id, String fileName) {
        this.link = link;
        this.text = text;
        this.id = id;
        this.fileName = fileName;
    }

    private String link;
    private String text;
    private Integer id;
    private String fileName;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
