package com.ibagroup.idas.chatbotserver.token;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class TokenController {

    @GetMapping("/token")
    @CrossOrigin(origins = "172.20.3.38:3000")
    @ApiOperation(value = "Retrieve token for this sesion")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Token has been retrieved successfully"),
            @ApiResponse(code = 400, message = "Can not retrieve token"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public String retrieveToken() {
        return "blablablablablabla";
    }

}
