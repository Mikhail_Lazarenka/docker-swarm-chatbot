package com.ibagroup.idas.chatbotserver.category.converter;

import com.ibagroup.idas.chatbotserver.category.controller.CategoryRq;
import com.ibagroup.idas.chatbotserver.category.repository.CategoryEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class CategoriesToEntityConverter implements Converter<List<CategoryRq>, List<CategoryEntity>> {

    @Override
    public List<CategoryEntity> convert(List<CategoryRq> categoriesRq) {
        List<CategoryEntity> categoryEntities = new ArrayList<>();
        for (CategoryRq categoryRq : categoriesRq) {
            CategoryEntity categoryEntity = new CategoryEntity();
            categoryEntity.setName(categoryRq.getName());
     /*       for (CategoryChildRs categoryChildRs : categoryRq.getCategoryChildRs()) {
                CategoryEntity categoryEntityChild = new CategoryEntity();
                categoryEntityChild.setId(categoryChildRs.getId());
                categoryEntityChild.setName(categoryChildRs.getName());
                //categoryEntityChild.setCategoryParentId(categoryEntity.getCategoryParentId());
                categoryEntities.add(categoryEntityChild);
            }*/
          /*  categoryEntity.setCategoryParentId(0);*/
            categoryEntities.add(categoryEntity);
        }

        return categoryEntities;
    }


}
