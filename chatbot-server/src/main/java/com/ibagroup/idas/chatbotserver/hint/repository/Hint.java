package com.ibagroup.idas.chatbotserver.hint.repository;

import javax.persistence.*;

@Entity(name = "hint")
public class Hint {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String link;

    private String text;

    private Integer categoryId;

    private String fileName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
