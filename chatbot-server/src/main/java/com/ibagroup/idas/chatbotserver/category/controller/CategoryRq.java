package com.ibagroup.idas.chatbotserver.category.controller;

import java.util.List;

public class CategoryRq {

    private String  name;
    private List<CategoryChildRq> categoryChildRs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategoryChildRq> getCategoryChildRs() {
        return categoryChildRs;
    }

    public void setCategoryChildRs(List<CategoryChildRq> categoryChildRs) {
        this.categoryChildRs = categoryChildRs;
    }
}
