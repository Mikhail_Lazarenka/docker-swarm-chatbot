package com.ibagroup.idas.chatbotserver.category.controller;

import java.util.List;

public class CategoryResp {

    private Integer id;
    private String  name;
    private List<SubCategoryRs> subCategories;

    public CategoryResp() {
    }

    public CategoryResp(Integer id, String name) {
        this.id = id;
        this.name = name;

    }

    public CategoryResp(Integer id, String name, List<SubCategoryRs> subCategories) {
        this.id = id;
        this.name = name;
        this.subCategories = subCategories;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubCategoryRs> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategoryRs> subCategories) {
        this.subCategories = subCategories;
    }
}
