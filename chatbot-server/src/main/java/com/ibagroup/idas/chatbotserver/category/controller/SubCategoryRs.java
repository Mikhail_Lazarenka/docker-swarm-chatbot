package com.ibagroup.idas.chatbotserver.category.controller;

public class SubCategoryRs {
    private Integer id;
    private String  name;


    public SubCategoryRs() {
    }

    public SubCategoryRs(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
