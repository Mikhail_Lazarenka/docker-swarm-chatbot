package com.ibagroup.idas.chatbotserver.hint.service;

import com.ibagroup.idas.chatbotserver.hint.controller.HintResp;
import com.ibagroup.idas.chatbotserver.hint.repository.Hint;
import com.ibagroup.idas.chatbotserver.hint.repository.HintRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HintService {

    @Autowired
    private HintRepository hintRepository;

    public List<HintResp> getHintsList() {
        Iterable<Hint> hints= hintRepository.findAll();
        List<HintResp> hintResps = new ArrayList<>();
        hints.iterator().forEachRemaining(hint ->  hintResps.add(new HintResp(hint.getLink(), hint.getText(), hint.getCategoryId(), hint.getFileName())));
        return hintResps;
    }

    public HintResp getHintById(Integer id) {
        Optional<Hint> hint = hintRepository.findById(id);
        return new HintResp(hint.get().getLink(), hint.get().getText(), hint.get().getCategoryId(), hint.get().getFileName());
    }

    public List<HintResp> getHintByCategoryId(Integer categoryId) {
        Iterable<Hint> hints = hintRepository.findByCategoryId(categoryId);
        List<HintResp> hintResps = new ArrayList<>();
        hints.iterator().forEachRemaining(hint ->  hintResps.add(new HintResp(hint.getLink(),hint.getText(), hint.getCategoryId(), hint.getFileName())));
        return hintResps;
    }
}
