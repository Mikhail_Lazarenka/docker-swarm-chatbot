package com.ibagroup.idas.chatbotserver.category.converter;

import com.ibagroup.idas.chatbotserver.category.controller.CategoryResp;
import com.ibagroup.idas.chatbotserver.category.repository.CategoryEntity;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CategoryToRespConverter implements Converter<Optional<CategoryEntity>, CategoryResp> {
    @Override
    public CategoryResp convert(Optional<CategoryEntity> categoryEntity) {
        CategoryResp categoryResp = null;
        if (categoryEntity.isPresent()) {
            categoryResp = new CategoryResp();
            categoryResp.setId(categoryEntity.get().getId());
            categoryResp.setName(categoryEntity.get().getName());
        }
        return categoryResp;
    }
}
