package com.ibagroup.idas.chatbotserver.category.repository;

import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<CategoryEntity,Integer> {

    CategoryEntity findByIdAndCategoryParentId(Integer id, Integer categoryParentId);

    Iterable<CategoryEntity> findByCategoryParentId(Integer categoryParentId);
}
