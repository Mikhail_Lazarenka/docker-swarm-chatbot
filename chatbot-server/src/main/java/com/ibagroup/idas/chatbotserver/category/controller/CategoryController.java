package com.ibagroup.idas.chatbotserver.category.controller;

import com.ibagroup.idas.chatbotserver.category.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories")
    @ApiOperation(value = "Get Lists of categories. if category parent is 0, its root category")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get Lists of categories successfully"),
            @ApiResponse(code = 400, message = "Can not get list categories"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @ResponseBody
    List<CategoryResp> getCategoryResp() {
        return categoryService.getCategoryList();
    }

    @GetMapping("/categoriesById")
    @ApiOperation(value = "Get category by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get category by id successfully"),
            @ApiResponse(code = 400, message = "Can not get list categories"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @ResponseBody CategoryResp getCategoryRespBId(@RequestParam Integer id) {
        return categoryService.getCategoryById(id);
    }

    @GetMapping("/subCategoriesById")
    @ApiOperation(value = "Get sub category by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get sub category by id successfully"),
            @ApiResponse(code = 400, message = "Can not get list sub categories"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @ResponseBody
    SubCategoryRs getSubCategoryRespBId(@RequestParam Integer subCategoryId, @RequestParam Integer categoryRootId) {
        return categoryService.getSubCategoryById(subCategoryId, categoryRootId );
    }

    @PostMapping("/addCategories")
    @ApiOperation(value = "add categories")
    @ApiResponses({
            @ApiResponse(code = 200, message = " add categories successfully"),
            @ApiResponse(code = 400, message = "Can not add categories"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public void addCategories(@RequestBody List<CategoryRq> categoryRqs) {
       categoryService.addCategories(categoryRqs);
    }

}
