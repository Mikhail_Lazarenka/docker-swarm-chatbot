package com.ibagroup.idas.chatbotserver.rating.repository;

import com.ibagroup.idas.chatbotserver.category.repository.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface RatingRepository extends CrudRepository<CategoryEntity,Long> {
}
