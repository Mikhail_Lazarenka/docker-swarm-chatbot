package com.ibagroup.idas.chatbotserver.hint.controller;

import com.ibagroup.idas.chatbotserver.hint.service.HintService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class HintController {

    @Autowired
    private HintService hintsService;

    @GetMapping("/hints")
    @CrossOrigin(origins = "172.20.3.38:3000")
    @ApiOperation(value = "Get Lists of hints. its docs or messages")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get Lists of hints successfully"),
            @ApiResponse(code = 400, message = "Can not get list hints"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @ResponseBody
    List<HintResp> getHints() {
        return hintsService.getHintsList();
    }

    @GetMapping("/hintsById")
    @CrossOrigin(origins = "172.20.3.38:3000")
    @ApiOperation(value = "Get hints by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get hints by id successfully"),
            @ApiResponse(code = 400, message = "Can not get list hints"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @ResponseBody
    HintResp getCategoryRespBId(@RequestParam Integer id) {
        return hintsService.getHintById(id);
    }

    @GetMapping("/hintsByCategoryId")
    @CrossOrigin(origins = "172.20.3.38:3000")
    @ApiOperation(value = "Get hints by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get hints by id successfully"),
            @ApiResponse(code = 400, message = "Can not get list hints"),
            @ApiResponse(code = 500, message = "Technical error occurred")
    })
    public @ResponseBody List<HintResp> getHintRespByCategoryId(@RequestParam Integer id) {
        return hintsService.getHintByCategoryId(id);
    }
}
